export type AmplifyDependentResourcesAttributes = {
    "api": {
        "AmplifyApp": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}