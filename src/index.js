import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { Amplify } from 'aws-amplify';
import awsExports from './aws-exports';
import { LoginForm } from './components/LoginForm'
import { ShowUsers } from './components/ShowUsers'
import 'bootstrap/dist/css/bootstrap.min.css'
Amplify.configure(awsExports);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
     <>
      <h1>myAmplifyApp</h1>

      <div className="App">
        <LoginForm/>
        <br /><br />
        <ShowUsers/>
      </div>
    </>
  </React.StrictMode>
);
reportWebVitals();
