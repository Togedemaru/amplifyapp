import React from 'react'
import { useState } from 'react'
import { DataStore } from '@aws-amplify/datastore';
import { Users } from '../models';

export const ShowUsers = () => {

    const [Usuarios, setUsuarios] = useState([]);

    const getData = async ( event ) => {
        const posts = await DataStore.query(Users);
        //const posts = await DataStore.query(Users, c => c.name.contains('Ricardo'));
        setUsuarios([...posts]);
    }


  return (
    <div>
        <button onClick={getData}>
            Mostrar Usuarios
        </button>
        <br /><br />
        <table class='table table-hover'>
        <thead>
            <tr>
                <td>
                    Id
                </td>
                <td>
                    Nombre
                </td>
                <td>
                    Apellido
                </td>
            </tr>
        </thead>
        <tbody>
        {Usuarios.map((key) => (
            <tr key={key.id}>
                <td>
                    {key.id}
                </td>
                <td>
                    {key.name}
                </td>
                <td>
                    {key.app}
                </td>
            </tr>
        ))}
        </tbody>
        </table>
    </div>
  )
}
