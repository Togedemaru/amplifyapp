import { useState } from "react"
import { DataStore } from '@aws-amplify/datastore';
import { Users } from '../models';


export const LoginForm = () => {

    const [inputValue, setinputValue] = useState({
        name: '',
        app: '',
    });

    const handleInputChance = ({ target }) => {
        const name = target.name;
        const value = target.value;
        console.log( target, value, name );
        setinputValue((prev) => {
            return {...prev, [name]: value}
        });
    };

    const onSubmit = async ( event ) => {
        console.log(inputValue);
        event.preventDefault();
        if(inputValue.name != 0 && inputValue.app != 0){
            alert('Usuario a agregar: ' + inputValue.name + ' ' + inputValue.app);
            await DataStore.save(
                new Users({
                    "name": inputValue.name,
                    "app":  inputValue.app,
                })
            );
        }else{
            alert('Imposible agregar un usuario sin datos');
        }
    };



    return (
        <form onSubmit = { onSubmit }>
            <input 
                type = "text"
                name = "name"
                placeholder = "Ej. Luisito"
                value = { inputValue.name }
                onChange= { handleInputChance }
            />
            <br/> <br/>
            <input 
                type = "text"
                name = "app"
                placeholder = "Ej.Ballesteros"
                value = { inputValue.app }
                onChange= { handleInputChance }
            />
            <br/> <br/>
            <button type="submit">
                Crear Usuario
            </button>
        </form>
    );
}